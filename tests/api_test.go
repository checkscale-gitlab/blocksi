package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"testing"
)

// TestRegisterHandler check register code
func TestRegisterHandler(t *testing.T) {
	reqBody, err := json.Marshal(map[string]string{
		"username": "ayush",
		"password": "ayush",
	})

	if err != nil {
		print(err)
	}
	resp, err := http.Post("127.0.0.1:8080/register",
		"application/json", bytes.NewBuffer(reqBody))
	if err != nil {
		print(err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		print(err)
	}
	fmt.Println(string(body))
}
