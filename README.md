# JWT Authentication in Go

Created the Golang backend for login using JWT authentication with the help of the [Medium tutorial](https://medium.com/@holy_abimz/authentication-in-golang-c0677bcce1a8)

Using [Go Modules](https://go.dev/blog/using-go-modules) package manager.

## Database
> Using postgresql docker image `postgres:12` and the name of the database is `TEST` 

> Tables are `users` and `contacts`

To start the backend app, perform the following step in order

1.  enter `go run main.go` to start server

Please find the example of using the API [here](https://gitlab.com/ayush-iitkgp/blocksi/-/blob/master/postman/blocksi.postman_collection.json)

Dockerized the go app using [this tutorial](https://dev.to/karanpratapsingh/dockerize-your-go-app-46pp)