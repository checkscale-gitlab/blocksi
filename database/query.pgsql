-- Show all the databses

SELECT datname
FROM pg_database;

-- Show all the tables in the database

SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog'
    AND schemaname != 'information_schema';

-- show the data in the table
SELECT *
FROM users;