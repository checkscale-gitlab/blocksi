package models

import "github.com/jinzhu/gorm"

//User struct declaration
type User struct {
	gorm.Model

	Username string `gorm:"type:varchar(100);unique_index" json:"username"`
	Password string `gorm:"not null" json:"password"`
}
