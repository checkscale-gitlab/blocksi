package controllers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/ayush-iitkgp/blocksi/models"

	"github.com/gorilla/mux"
)

//CreateContact create a new contact for a user
func CreateContact(w http.ResponseWriter, r *http.Request) {
	// db.Preload("auths")
	contact := &models.Contact{}
	// fmt.Printf(r.Body)
	json.NewDecoder(r.Body).Decode(contact)

	createdContact := db.Create(contact)
	var errMessage = createdContact.Error

	if createdContact.Error != nil {
		fmt.Println(errMessage)
	}
	json.NewEncoder(w).Encode(createdContact)
}

//GetAllContacts gets all the contacts for a user
func GetAllContacts(w http.ResponseWriter, r *http.Request) {
	var contact []models.Contact
	db.Find(&contact)
	// db.Find(&contact)
	json.NewEncoder(w).Encode(contact)
}

// GetContact gets a contacts for the user using the id
func GetContact(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var contactID = params["id"]
	// fmt.Printf(id)
	log.Printf("Id is %s", contactID)
	var contact models.Contact
	db.First(&contact, contactID)
	json.NewEncoder(w).Encode(&contact)
}

// UpdateContact updates a contacts for the user using the id
func UpdateContact(w http.ResponseWriter, r *http.Request) {
	contact := &models.Contact{}
	params := mux.Vars(r)
	var contactID = params["id"]
	log.Printf("Id is %s", contactID)
	db.First(&contact, contactID)
	json.NewDecoder(r.Body).Decode(contact)
	db.Save(&contact)
	json.NewEncoder(w).Encode(&contact)
}

// DeleteContact deletes a contacts for the user using the id
func DeleteContact(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	var contactID = params["id"]
	log.Printf("Id is %s", contactID)
	var contact models.Contact
	db.First(&contact, contactID)
	db.Delete(&contact)
	json.NewEncoder(w).Encode("Contact deleted")
}
