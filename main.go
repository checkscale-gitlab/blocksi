package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/ayush-iitkgp/blocksi/routes"

	"github.com/gorilla/handlers"
	"github.com/joho/godotenv"
)

func main() {
	e := godotenv.Load("env/.env.app")

	if e != nil {
		log.Fatal("Error loading env/.env.app file")
	}
	fmt.Println(e)

	port := os.Getenv("PORT")

	headersOK := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type", "Access-Control-Request-Headers"})
	originsOK := handlers.AllowedOrigins([]string{"*"})
	methodsOK := handlers.AllowedMethods([]string{"GET", "POST", "OPTIONS", "DELETE", "PUT"})

	// Handle routes
	r := routes.Handlers()
	http.Handle("/", r)
	// serve
	log.Printf("Server up on port '%s'", port)
	log.Fatal(http.ListenAndServe(":"+port, handlers.CORS(headersOK, originsOK, methodsOK)(r)))
}
