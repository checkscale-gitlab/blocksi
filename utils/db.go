package utils

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/ayush-iitkgp/blocksi/models"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" //Gorm postgres dialect interface
	"github.com/joho/godotenv"
)

//ConnectDB function: Make database connection
func ConnectDB() *gorm.DB {

	//Load environmenatal variables
	err := godotenv.Load("env/.env.app")

	if err != nil {
		log.Fatal("Error loading env/.env.app file")
	}

	username := os.Getenv("databaseUser")
	password := os.Getenv("databasePassword")
	databaseName := os.Getenv("databaseName")
	databaseHost := os.Getenv("databaseHost")
	port := os.Getenv("databasePort")

	fmt.Printf("host=%s:%s user=%s dbname=%s sslmode=disable password=%s\n", databaseHost, port, username, databaseName, password)
	//Define DB connection string
	dbURI := fmt.Sprintf("host=%s user=%s dbname=%s sslmode=disable password=%s", databaseHost, username, databaseName, password)

	//connect to db URI
	db, err := gorm.Open("postgres", dbURI)

	if err != nil {
		fmt.Println("error", err)
		panic(err)
	}
	// close db when not in use
	// defer db.Close()

	// Migrate the user schema
	db.AutoMigrate(&models.User{})
	// Migrate contacts table
	db.AutoMigrate(&models.Contact{})
	fmt.Println("Successfully connected!", db)
	return db
}
